﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public enum Country { Korea, China, Japan }

    public struct Employee
    {
        public int birthYear;
        public string name;
    }

    public class Employ
    {
        public int birthYear;
        public string name;
    }

    public class Product
    {
        public const int constPrice = 1000;
        public readonly int readonlyPrice;

        public Product()
        {
            this.readonlyPrice = 2000;
        }

        public Product(int price)
        {
            this.readonlyPrice = price;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!!!");

            //열거형
            Country location1 = Country.Korea;
            Country location2 = Country.China;
            int location3 = (int)location2;

            Console.WriteLine("location1 : {0}", location1);
            Console.WriteLine("location2 : {0}", location2);
            Console.WriteLine("location3(int) : {0}", location3);
            Console.WriteLine("location3(Country) : {0}", Country.Japan);

            //null able
            int? number = null;
            int defaultNumber1 = (number == null) ? 0 : (int)number;

            if (number.HasValue)
            {
                Console.WriteLine(number.Value);
            }
            else
            {
                Console.WriteLine("값이 null입니다.");
            }
            Console.WriteLine("defaultNumber1 : {0}", defaultNumber1);

            int defaultNUmber2 = number ?? 0;
            Console.WriteLine("defaultNumber2 : {0}", defaultNUmber2);

            //int형
            int number3 = 123;
            System.Int32 number4 = 123;
            Console.WriteLine("number3 : {0}", number3);
            Console.WriteLine("number4 : {0}", number4);
            //double형
            double number5 = 123d;
            double number6 = 123;
            Console.WriteLine("number5 : {0}", number5);
            Console.WriteLine("number6 : {0}", number6);

            //스트럭처
            Employee emp1 = new Employee();
            emp1.birthYear = 1999;
            emp1.name = "홍길동";

            Employee emp2 = emp1;
            Console.WriteLine("============ 스트럭처 ==================");
            Console.WriteLine("emp1 birthYear : {0}", emp1.birthYear);
            Console.WriteLine("emp2 birthYear : {0}", emp2.birthYear);

            emp1.birthYear = 2000;
            Console.WriteLine("= emp1의 birthYear을 2000으로 변경 =");
            Console.WriteLine("emp1 birthYear : {0}", emp1.birthYear);
            Console.WriteLine("emp2 birthYear : {0}", emp2.birthYear);

            //클래스
            Employ empy1 = new Employ();
            empy1.birthYear = 1999;
            empy1.name = "홍길동";

            Employ empy2 = empy1;
            Console.WriteLine("============ 클래스 ==================");
            Console.WriteLine("emp1 birthYear : {0}", empy1.birthYear);
            Console.WriteLine("emp2 birthYear : {0}", empy2.birthYear);

            empy1.birthYear = 2000;
            Console.WriteLine("= emp1의 birthYear을 2000으로 변경 =");
            Console.WriteLine("emp1 birthYear : {0}", empy1.birthYear);
            Console.WriteLine("emp2 birthYear : {0}", empy2.birthYear);

            //const, readonly
            Console.WriteLine("============ const, readonly ==================");
            Console.WriteLine("ConstPrice : {0}", Product.constPrice);

            Product pro1 = new Product();
            Console.WriteLine("new Product() readonlyPrice : {0}", pro1.readonlyPrice);

            Product pro2 = new Product(3000);
            Console.WriteLine("new Prpro2oduct(3000) readonlyPrice : {0}", pro2.readonlyPrice);

            //형변환
            string strNumber = "1234";
            int num1 = Convert.ToInt32(strNumber);
            int num2 = int.Parse(strNumber);

            Console.WriteLine("============ 형변환 ==================");
            Console.WriteLine("number1 : {0}", num1);
            Console.WriteLine("number2 : {0}", num2);

            Console.Write("Press any key to continue...");
            Console.ReadKey(true);
        }
    }
}
